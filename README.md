# 2019-MPS-raphael

EDAN70 Project in Compilers for 2019 Fall semester.

SimpliC implementation into MPS tool from JetBrains.

Author : [Raphael Castanier](mailto:raphael.castanier@grenoble-inp.org)


## Where to find things?

- Project files contain information about EDAN70 project :
    - [Changelog](CHANGELOG.md) contains human friendly history of project
    - [LICENSE](LICENSE) contains copyright information
    - [TODO list](TODO.md) contain personal task list
    - [Week by week](WEEK_BY_WEEK.md) contains required tasks to be done during course period
- [Report](report/paper.tex) is located in [its folder](report)
- [Presentation](presentation/paper.tex) is located in [its folder](presentation)
- [MPS-Project](MPS-Project) contains MPS Project, including
    - SimpliC Implementation
    - Sandbox program
    - Standalone IDE build scripts


## Quick installation instructions

1. [Download JetBrains Toolbox](https://www.jetbrains.com/toolbox-app/) and install it on your computer.
2. From Toolbox, install MPS.
3. Open [MPS-Project](MPS-Project) folder in MPS.
4. (Optional) Run "Rebuild project" if needed


## How to explore SimpliC implementation

![Project structure](project_structure.jpg)

After MPS installation and project opening, you can inspect SimpliC implementation by opening the language aspects in the SimpliC folder (yellow L diamond).
There are several language aspects :
- Structure : language nodes (AST-like nodes)
- Editor : aspects for language edition
- TextGen : assembly text generation from program


## How to edit sandbox programs

You can test SimpliC editor in the sandbox program (third purple L diamond).
Edit existing programs using editor (find help [here](https://www.jetbrains.com/help/mps/commanding-the-editor.html) for editor usage) or you can create new SimpliC programs.


## How to generate assembly from programs (SimpliC compilation)

You can generate assembly from your SimpliC program.
To do this, right-ckick on your program and select `Preview generated text`.
IDE should produce a `program.s` file containing assembly instructions.


## How to build SimpliC IDE

![Project structure build](project_structure_build.jpg)

You need to download and install [MPS generic distribution](https://www.jetbrains.com/mps/download/#section=zip).
This distribution includes artefacts needed for compilation.
Then you can re-open [MPS-Project](MPS-Project) in MPS generic distribution and build the standalone IDE.
To do this, right-click on SimpliC-IDEDistribution then select `Run script`.

If build succeeded, you will find the produced packages in the [build folder](MPS-Project\build\artifacts\SimpliC-IDEDistribution).
Otherwise, refer to Internet Help.


## Releases

See [Changelog](CHANGELOG.md).


## License

This repository is covered by the license BSD 2-clause, see [LICENSE](LICENSE).


## Credits

None for now.
