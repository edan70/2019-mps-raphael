# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.0.10] - 2020-01-30
### Changed
- report fix according to feedback


## [0.0.9] - 2020-01-25
### Changed
- report fix according to feedback


## [0.0.8] - 2020-01-10
### Added
- report release


## [0.0.7] - 2019-12-20
### Added
- slides for presentation


## [0.0.6] - 2019-12-16
### Added
- slides for presentation
- demo Sandbox program

### Changed
- SimpliC implementation (editor, text gen, built-in function)


## [0.0.5] - 2019-12-06
### Changed
- update report


## [0.0.4] - 2019-11-29
### Added
- text generation for SimpliC to assembly
- standalone IDE for SimpliC

### Changed
- update README instructions


## [0.0.3] - 2019-11-22
### Changed
- report outline fixed
- complete SimpliC concepts
- complete SimpliC editors


## [0.0.2] - 2019-11-15
### Added
- basic SimpliC concepts
- basic SimpliC editors
- SimpliC sandbox
- report template
- report first version
- installation instructions

### Changed
- renamed Changelog to CHANGELOG


## [0.0.1] - 2019-11-08
### Added
- README, CHANGELOG, LICENSE, TODO
- MPS Project
- Report
- minimal SimpliC implementation


## [0.0.0] - 2019-11-05
### Added
- Initial content


[0.0.10]: https://bitbucket.org/edan70/2019-mps-raphael/src/0.0.10/
[0.0.9]: https://bitbucket.org/edan70/2019-mps-raphael/src/0.0.9/
[0.0.8]: https://bitbucket.org/edan70/2019-mps-raphael/src/0.0.8/
[0.0.7]: https://bitbucket.org/edan70/2019-mps-raphael/src/0.0.7/
[0.0.6]: https://bitbucket.org/edan70/2019-mps-raphael/src/0.0.6/
[0.0.5]: https://bitbucket.org/edan70/2019-mps-raphael/src/0.0.5/
[0.0.4]: https://bitbucket.org/edan70/2019-mps-raphael/src/0.0.4/
[0.0.3]: https://bitbucket.org/edan70/2019-mps-raphael/src/0.0.3/
[0.0.2]: https://bitbucket.org/edan70/2019-mps-raphael/src/0.0.2/
[0.0.1]: https://bitbucket.org/edan70/2019-mps-raphael/src/0.0.1/
[0.0.0]: https://bitbucket.org/edan70/2019-mps-raphael/src/0.0.0/
