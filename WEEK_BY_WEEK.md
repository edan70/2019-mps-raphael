# Week by week tasks

## Week 1
- [x] Introductory course meeting, Tuesday 10-12, Nov 5, E:3315
- [x] Select project
- [x] Meet with supervisor
    - [x] Discuss project content: motivation, what to implement, how to evaluate,
related work
    - [x] Advice about getting started with implementation
    - [x] Advice about writing initial report due next week
    - [x] Concrete plan for next week
- [x] Sign up at Moodle
- [x] Skim through instructions about Report
- [x] Start writing initial report with introduction and outline
- [x] Start reading related papers
- [x] Get started on implementation

## Week 2
- [x] Continue work on implementation and first version of report
- [x] Read about Finding Related Work
- [x] Meet with supervisor
    - [x] Progress since last meeting
    - [x] Advice about first version of report
    - [x] Advice about implementation to create demo due next week
    - [x] Concrete plan for next week
- [x] Upload initial report in Moodle by Friday, Nov 15

## Week 3
- [x] Continue work on implementation to create demo
- [x] Read about Releases
- [x] Meet with supervisor
    - [x] Progress since last meeting
    - [x] Feedback on initial report
    - [x] Show demo to supervisor
    - [x] Advice about continued development
    - [x] Advice about alpha release
    - [x] Concrete plan for next week
- [x] Upload improved initial report by Friday, Nov 22

## Week 4
- [x] Continue work on implementation and release
- [x] Read about Evaluation
- [x] Meet with supervisor
    - [x] Progress since last meeting
    - [x] Feedback on report
    - [x] Discuss evaluation
    - [x] Show improved demo to supervisor
    - [x] Advice about writing about the solution in the report
    - [x] Concrete plan for next week
- [x] Do alpha release and upload info about it on Moodle by Friday, Nov 29

## Week 5
- [x] Continue work on implementation and paper
- [x] Read about Report in more detail
- [x] Meet with supervisor
    - [x] Progress since last meeting
    - [x] Feedback on first release
    - [x] Discuss more related work
    - [x] Discuss presentation
    - [x] Concrete plan for next week
- [x] Upload middle version of report, including solution description, by Friday, Dec 6

## Week 6
- [x] Continue work on implementation and paper
- [x] Read about Presentation
- [x] Work on draft of slides for presentation
- [x] Meet with supervisor
    - [x] Progress since last meeting
    - [x] Feedback on new version of report
    - [x] Discuss draft of presentation slides
    - [x] Concrete plan for next week
- [x] Upload draft of slides by Friday, Dec 13

## Week 7
- [x] Continue work on implementation and presentation
- [x] Present your work at one of the seminar sessions. Listen to other presentations at
the session.
- [x] Upload final version of slides by Friday, Dec 20

## January 2020
- [x] Finalize report and release. Make sure to follow all the instructions.
- [x] Upload final report and release by Wednesday, Jan 15
