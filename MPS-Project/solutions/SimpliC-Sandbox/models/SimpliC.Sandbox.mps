<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:abc00057-aca0-4516-b94d-1a3aa01cee9e(SimpliC.Sandbox)">
  <persistence version="9" />
  <languages>
    <use id="11970950-6d41-49d2-a07e-65645b6ac140" name="SimpliC" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="11970950-6d41-49d2-a07e-65645b6ac140" name="SimpliC">
      <concept id="6606968234980213479" name="SimpliC.structure.BuiltInPrint" flags="ng" index="2rYqxX" />
      <concept id="9168226408505491819" name="SimpliC.structure.IntType" flags="ng" index="2$rPgW" />
      <concept id="9168226408505491818" name="SimpliC.structure.VoidType" flags="ng" index="2$rPgX" />
      <concept id="9168226408505077547" name="SimpliC.structure.CommentStatement" flags="ng" index="2$sfDW">
        <property id="9168226408505160471" name="text" index="2$syp0" />
      </concept>
      <concept id="7668420699291494155" name="SimpliC.structure.LessEqual" flags="ng" index="1jVxjO" />
      <concept id="7668420699291494151" name="SimpliC.structure.Substraction" flags="ng" index="1jVxjS" />
      <concept id="7668420699291494147" name="SimpliC.structure.Multiplication" flags="ng" index="1jVxjW" />
      <concept id="7668420699291494141" name="SimpliC.structure.BinaryEpression" flags="ng" index="1jVxk2">
        <child id="7668420699291494144" name="Right" index="1jVxjZ" />
        <child id="7668420699291494142" name="Left" index="1jVxk1" />
      </concept>
      <concept id="7668420699291494136" name="SimpliC.structure.FunctionArgument" flags="ng" index="1jVxk7">
        <child id="2583485096447935258" name="Expression" index="33$u4r" />
      </concept>
      <concept id="7668420699291494126" name="SimpliC.structure.FunctionCallExpression" flags="ng" index="1jVxkh">
        <reference id="6531486960825748393" name="FunctionDeclaration" index="11IsHw" />
        <child id="7668420699291494133" name="FunctionArgument" index="1jVxka" />
      </concept>
      <concept id="7668420699291494120" name="SimpliC.structure.ReturnStatement" flags="ng" index="1jVxkn">
        <child id="7668420699291494121" name="Expression" index="1jVxkm" />
      </concept>
      <concept id="7668420699291494109" name="SimpliC.structure.ElseStatement" flags="ng" index="1jVxky">
        <child id="7668420699291494112" name="Block" index="1jVxkv" />
      </concept>
      <concept id="7668420699291494099" name="SimpliC.structure.IfStatement" flags="ng" index="1jVxkG">
        <child id="7668420699291494105" name="ElseStatement" index="1jVxkA" />
        <child id="7668420699291494102" name="Block" index="1jVxkD" />
        <child id="7668420699291494100" name="Condition" index="1jVxkF" />
      </concept>
      <concept id="7668420699291494088" name="SimpliC.structure.FunctionCallStatement" flags="ng" index="1jVxkR">
        <child id="7668420699291494089" name="FunctionCallExpression" index="1jVxkQ" />
      </concept>
      <concept id="4285788564039754428" name="SimpliC.structure.IdUse" flags="ng" index="3lvOX1">
        <reference id="6432465198072717954" name="decl" index="0qPu6" />
      </concept>
      <concept id="4285788564039754397" name="SimpliC.structure.Numeral" flags="ng" index="3lvOXw">
        <property id="4285788564039754400" name="Numeral" index="3lvOXt" />
      </concept>
      <concept id="4285788564039754385" name="SimpliC.structure.IdDeclaration" flags="ng" index="3lvOXG">
        <child id="9168226408505492034" name="type" index="2$rPsl" />
      </concept>
      <concept id="4285788564039754382" name="SimpliC.structure.Block" flags="ng" index="3lvOXN">
        <child id="4285788564039754383" name="statementList" index="3lvOXM" />
      </concept>
      <concept id="4285788564039754379" name="SimpliC.structure.Param" flags="ng" index="3lvOXQ">
        <child id="5411749480204969695" name="IdDeclaration" index="2e7We_" />
      </concept>
      <concept id="4285788564039754258" name="SimpliC.structure.FunctionDeclaration" flags="ng" index="3lvOZJ">
        <child id="9168226408505521993" name="type" index="2$rU8u" />
        <child id="4285788564039754375" name="block" index="3lvOXU" />
        <child id="4285788564039754370" name="param" index="3lvOXZ" />
      </concept>
      <concept id="4285788564039754249" name="SimpliC.structure.Program" flags="ng" index="3lvOZO">
        <child id="4285788564039754254" name="functionDecl" index="3lvOZN" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="3lvOZO" id="20l7azhjAX3">
    <node concept="3lvOZJ" id="20l7azhjAX7" role="3lvOZN">
      <property role="TrG5h" value="main" />
      <node concept="2$rPgX" id="20l7azhjAX8" role="2$rU8u" />
      <node concept="3lvOXN" id="20l7azhjAX9" role="3lvOXU">
        <node concept="1jVxkR" id="20l7azhjB1U" role="3lvOXM">
          <node concept="1jVxkh" id="20l7azhjB1Y" role="1jVxkQ">
            <ref role="11IsHw" node="20l7azhjAX4" resolve="print" />
            <node concept="1jVxk7" id="20l7azhjB21" role="1jVxka">
              <node concept="1jVxkh" id="20l7azhjB2d" role="33$u4r">
                <ref role="11IsHw" node="20l7azhjAXH" resolve="fac" />
                <node concept="1jVxk7" id="20l7azhjB2g" role="1jVxka">
                  <node concept="3lvOXw" id="20l7azhjB2k" role="33$u4r">
                    <property role="3lvOXt" value="7" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3lvOZJ" id="20l7azhjAXH" role="3lvOZN">
      <property role="TrG5h" value="fac" />
      <node concept="3lvOXQ" id="20l7azhjAYg" role="3lvOXZ">
        <node concept="3lvOXG" id="20l7azhjAYh" role="2e7We_">
          <property role="TrG5h" value="n" />
          <node concept="2$rPgW" id="20l7azhjAYu" role="2$rPsl" />
        </node>
      </node>
      <node concept="2$rPgW" id="20l7azhjAXX" role="2$rU8u" />
      <node concept="3lvOXN" id="20l7azhjAXJ" role="3lvOXU">
        <node concept="2$sfDW" id="20l7azhjAYD" role="3lvOXM">
          <property role="2$syp0" value="stop condition" />
        </node>
        <node concept="1jVxkG" id="20l7azhjAZ2" role="3lvOXM">
          <node concept="1jVxjO" id="20l7azhjAZi" role="1jVxkF">
            <node concept="3lvOX1" id="20l7azhjAZr" role="1jVxk1">
              <ref role="0qPu6" node="20l7azhjAYh" resolve="n" />
            </node>
            <node concept="3lvOXw" id="20l7azhjAZz" role="1jVxjZ">
              <property role="3lvOXt" value="1" />
            </node>
          </node>
          <node concept="3lvOXN" id="20l7azhjAZ6" role="1jVxkD">
            <node concept="1jVxkn" id="20l7azhjAZE" role="3lvOXM">
              <node concept="3lvOX1" id="20l7azhjAZK" role="1jVxkm">
                <ref role="0qPu6" node="20l7azhjAYh" resolve="n" />
              </node>
            </node>
          </node>
          <node concept="1jVxky" id="20l7azhjAZ8" role="1jVxkA">
            <node concept="3lvOXN" id="20l7azhjAZa" role="1jVxkv" />
          </node>
        </node>
        <node concept="2$sfDW" id="20l7azhjB0d" role="3lvOXM">
          <property role="2$syp0" value="recursive call" />
        </node>
        <node concept="1jVxkn" id="20l7azhjB0B" role="3lvOXM">
          <node concept="1jVxjW" id="20l7azhjB0P" role="1jVxkm">
            <node concept="1jVxkh" id="20l7azhjB0V" role="1jVxk1">
              <ref role="11IsHw" node="20l7azhjAXH" resolve="fac" />
              <node concept="1jVxk7" id="20l7azhjB0Y" role="1jVxka">
                <node concept="1jVxjS" id="20l7azhjB12" role="33$u4r">
                  <node concept="3lvOX1" id="20l7azhjB1b" role="1jVxk1">
                    <ref role="0qPu6" node="20l7azhjAYh" resolve="n" />
                  </node>
                  <node concept="3lvOXw" id="20l7azhjB1l" role="1jVxjZ">
                    <property role="3lvOXt" value="1" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3lvOX1" id="20l7azhjB1F" role="1jVxjZ">
              <ref role="0qPu6" node="20l7azhjAYh" resolve="n" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2rYqxX" id="20l7azhjAX4" role="3lvOZN">
      <property role="TrG5h" value="print" />
      <node concept="2$rPgX" id="20l7azhjAX5" role="2$rU8u" />
      <node concept="3lvOXN" id="20l7azhjAX6" role="3lvOXU" />
    </node>
  </node>
</model>

