<?xml version="1.0" encoding="UTF-8"?>
<solution name="SimpliC-Sandbox" uuid="45066bd4-4b04-413a-acf2-5e22e79f0b48" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <facets>
    <facet type="java">
      <classes generated="true" path="${module}/classes_gen" />
    </facet>
  </facets>
  <sourcePath />
  <languageVersions>
    <language slang="l:11970950-6d41-49d2-a07e-65645b6ac140:SimpliC" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="2" />
  </languageVersions>
  <dependencyVersions>
    <module reference="45066bd4-4b04-413a-acf2-5e22e79f0b48(SimpliC-Sandbox)" version="0" />
  </dependencyVersions>
</solution>

