<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:ad98cc0b-b93a-43e9-9ddc-fe45c225db92(SimpliC.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="4" />
    <devkit ref="00000000-0000-4000-0000-1de82b3a4936(jetbrains.mps.devkit.aspect.typesystem)" />
  </languages>
  <imports>
    <import index="417" ref="r:c812ec5f-d6df-4351-b111-e327e3950242(SimpliC.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1185788614172" name="jetbrains.mps.lang.typesystem.structure.NormalTypeClause" flags="ng" index="mw_s8">
        <child id="1185788644032" name="normalType" index="mwGJk" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174643105530" name="jetbrains.mps.lang.typesystem.structure.InferenceRule" flags="ig" index="1YbPZF" />
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
      <concept id="1174657487114" name="jetbrains.mps.lang.typesystem.structure.TypeOfExpression" flags="nn" index="1Z2H0r">
        <child id="1174657509053" name="term" index="1Z2MuG" />
      </concept>
      <concept id="1174658326157" name="jetbrains.mps.lang.typesystem.structure.CreateEquationStatement" flags="nn" index="1Z5TYs" />
      <concept id="1174660718586" name="jetbrains.mps.lang.typesystem.structure.AbstractEquationStatement" flags="nn" index="1Zf1VF">
        <child id="1174660783413" name="leftExpression" index="1ZfhK$" />
        <child id="1174660783414" name="rightExpression" index="1ZfhKB" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1YbPZF" id="7WW5lvrkKzC">
    <property role="TrG5h" value="typeof_Numeral" />
    <node concept="3clFbS" id="7WW5lvrkKzD" role="18ibNy">
      <node concept="1Z5TYs" id="7WW5lvrkKSU" role="3cqZAp">
        <node concept="mw_s8" id="7WW5lvrkKT4" role="1ZfhK$">
          <node concept="1Z2H0r" id="7WW5lvrkKT0" role="mwGJk">
            <node concept="1YBJjd" id="7WW5lvrkKTl" role="1Z2MuG">
              <ref role="1YBMHb" node="7WW5lvrkKSL" resolve="numeral" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="7WW5lvrkKUc" role="1ZfhKB">
          <node concept="1YBJjd" id="7WW5lvrkMvq" role="mwGJk">
            <ref role="1YBMHb" node="7WW5lvrkKSL" resolve="numeral" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7WW5lvrkKSL" role="1YuTPh">
      <property role="TrG5h" value="numeral" />
      <ref role="1YaFvo" to="417:3HUbvRI1mat" resolve="Numeral" />
    </node>
  </node>
  <node concept="1YbPZF" id="7WW5lvrkLjL">
    <property role="TrG5h" value="typeof_IdUse" />
    <node concept="3clFbS" id="7WW5lvrkLjM" role="18ibNy">
      <node concept="1Z5TYs" id="7WW5lvrkLvU" role="3cqZAp">
        <node concept="mw_s8" id="7WW5lvrkLwe" role="1ZfhKB">
          <node concept="1Z2H0r" id="7WW5lvrkLwa" role="mwGJk">
            <node concept="2OqwBi" id="7WW5lvrkLCE" role="1Z2MuG">
              <node concept="1YBJjd" id="7WW5lvrkLwv" role="2Oq$k0">
                <ref role="1YBMHb" node="7WW5lvrkLjO" resolve="idUse" />
              </node>
              <node concept="3TrEf2" id="7WW5lvrkLMB" role="2OqNvi">
                <ref role="3Tt5mk" to="417:5_4HghpbYa2" resolve="decl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="7WW5lvrkLvX" role="1ZfhK$">
          <node concept="1Z2H0r" id="7WW5lvrkLjX" role="mwGJk">
            <node concept="1YBJjd" id="7WW5lvrkLlL" role="1Z2MuG">
              <ref role="1YBMHb" node="7WW5lvrkLjO" resolve="idUse" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7WW5lvrkLjO" role="1YuTPh">
      <property role="TrG5h" value="idUse" />
      <ref role="1YaFvo" to="417:3HUbvRI1maW" resolve="IdUse" />
    </node>
  </node>
  <node concept="1YbPZF" id="7WW5lvrkLPF">
    <property role="TrG5h" value="typeof_IdDeclaration" />
    <node concept="3clFbS" id="7WW5lvrkLPG" role="18ibNy">
      <node concept="1Z5TYs" id="7WW5lvrkLYZ" role="3cqZAp">
        <node concept="mw_s8" id="7WW5lvrkLZh" role="1ZfhKB">
          <node concept="1Z2H0r" id="7WW5lvrkPvd" role="mwGJk">
            <node concept="2OqwBi" id="7WW5lvrkPCY" role="1Z2MuG">
              <node concept="1YBJjd" id="7WW5lvrkPvF" role="2Oq$k0">
                <ref role="1YBMHb" node="7WW5lvrkLPI" resolve="idDeclaration" />
              </node>
              <node concept="3TrEf2" id="7WW5lvrkQ0P" role="2OqNvi">
                <ref role="3Tt5mk" to="417:7WW5lvrkO92" resolve="type" />
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="7WW5lvrkLZ2" role="1ZfhK$">
          <node concept="1Z2H0r" id="7WW5lvrkLRU" role="mwGJk">
            <node concept="1YBJjd" id="7WW5lvrkLRV" role="1Z2MuG">
              <ref role="1YBMHb" node="7WW5lvrkLPI" resolve="idDeclaration" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7WW5lvrkLPI" role="1YuTPh">
      <property role="TrG5h" value="idDeclaration" />
      <ref role="1YaFvo" to="417:3HUbvRI1mah" resolve="IdDeclaration" />
    </node>
  </node>
  <node concept="1YbPZF" id="7WW5lvrlgEU">
    <property role="TrG5h" value="typeof_FunctionDeclaration" />
    <node concept="3clFbS" id="7WW5lvrlgEV" role="18ibNy">
      <node concept="1Z5TYs" id="7WW5lvrlgO2" role="3cqZAp">
        <node concept="mw_s8" id="7WW5lvrlgOm" role="1ZfhKB">
          <node concept="1Z2H0r" id="7WW5lvrlgOi" role="mwGJk">
            <node concept="2OqwBi" id="7WW5lvrlgXn" role="1Z2MuG">
              <node concept="1YBJjd" id="7WW5lvrlgOB" role="2Oq$k0">
                <ref role="1YBMHb" node="7WW5lvrlgEX" resolve="functionDeclaration" />
              </node>
              <node concept="3TrEf2" id="7WW5lvrlh8m" role="2OqNvi">
                <ref role="3Tt5mk" to="417:7WW5lvrkVt9" resolve="type" />
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="7WW5lvrlgO5" role="1ZfhK$">
          <node concept="1Z2H0r" id="7WW5lvrlgF6" role="mwGJk">
            <node concept="1YBJjd" id="7WW5lvrlgGU" role="1Z2MuG">
              <ref role="1YBMHb" node="7WW5lvrlgEX" resolve="functionDeclaration" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7WW5lvrlgEX" role="1YuTPh">
      <property role="TrG5h" value="functionDeclaration" />
      <ref role="1YaFvo" to="417:3HUbvRI1m8i" resolve="FunctionDeclaration" />
    </node>
  </node>
</model>

