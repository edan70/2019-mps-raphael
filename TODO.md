# TODO list for this Project

## SimpliC implementation
- [x] Add built-in functions print
- [ ] Add built-in functions read
- [ ] Improve expression use
- [ ] Add parenthesis
- [ ] Add rule priority
- [ ] Manage labels
- [ ] Add diagram editor
- [x] Add comments
- [x] Implement type system
- [ ] Improve TypeSystem for expressions
- [ ] Improve Unique names for id declarations (according to scope rules)
    See https://www.jetbrains.com/help/mps/common-language-patterns.html#uniquenames
- [ ] Fix scope rules for variable/function use
    See https://www.jetbrains.com/help/mps/scopes.html
- [ ] Fix if/else statement editor
- [ ] Define reusable styles for keywords, IDs...
    See https://www.jetbrains.com/help/mps/editor-cookbook.html#definingandreusingstyles
- [ ] Implement while statement
- [ ] Fix text generation for label
- [ ] Fix text generation for expressions
- [ ] Fix numeral use
